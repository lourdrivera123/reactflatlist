import React from 'react';
import { View, FlatList, Text, ActivityIndicator } from 'react-native';
import { List, ListItem } from 'react-native-elements';

export default class App extends React.Component {
  state = {
    data: [],
    page: 0,
    loading: false,
  };

  componentWillMount() {
    this.fetchData();
  }

  fetchData = async () => {
    const response = await fetch(`https://randomuser.me/api?results=15&seed=hi&page=${this.state.page}`);
    const json = await response.json();
    this.setState(state => ({ 
      data: [...state.data, ...json.results],
      loading: false
    }));
  };

  handleEnd = () => {
    this.setState(state => ({ page: state.page + 1 }), () => this.fetchData());
  };

  render() {
    return (
      <View>
        <List>
          <FlatList
            data={this.state.data}
            keyExtractor={(item, index) => index}
            onEndReached={() => this.handleEnd()}
            onEndReachedThreshold={50}
            ListFooterComponent={() => <ActivityIndicator size='large' animating />}
            renderItem={({ item }) => 
              <ListItem
                roundAvatar
                avatar={{ uri: item.picture.thumbnail }}
                title={`${item.name.first} ${item.name.last}`}
              />}
          />
        </List>
      </View>
    );
  }
}
